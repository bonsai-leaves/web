import resources from "./data/resources"
import categories from "./data/categories"

export type CategoryId = string
export type Category = {
  id: CategoryId
  name: string
  description: string
}
export type Categories = Array<Category>

export type Resource = {
  name: string
  categories: Array<CategoryId>
  links: Array<{ name: string; url: string }>
  date?: string
  location?: string
}
export type Resources = Array<Resource>

const alphabetically = (a: { name: string }, b: { name: string }) => {
  const nameA = a.name.toLocaleLowerCase()
  const nameB = b.name.toLocaleLowerCase()

  if (nameA < nameB) {
    return -1
  }
  if (nameA > nameB) {
    return 1
  }

  return 0
}

const service = {
  fetchResources: (): Resources => {
    return resources.sort(alphabetically)
  },
  fetchCategories: (): Categories => {
    return categories.sort(alphabetically)
  },
}

export default service;
