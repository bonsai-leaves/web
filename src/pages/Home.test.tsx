import React from "react"
import { render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import Home from "./Home"
import { Category } from "../Api"
import { Router } from "react-router-dom"
import { MemoryHistory, createMemoryHistory } from "history"

describe("Home", () => {
  test("renders category title and description", async () => {
    const category = aCategory()

    renderPage(<Home />)

    expect(await screen.queryByText(category.name)).toBeInTheDocument()
    expect(await screen.queryByText(category.description)).toBeInTheDocument()
  })

  test("navigates to category resources", async () => {
    const category = aCategory()
    const history = createMemoryHistory()
    renderPage(<Home />, history)

    await userEvent.click(screen.getByText(category.name))

    expect(history.location.pathname).toBe(`/${category.id}`)
  })

  const aCategory = (): Category => {
    return {
      id: "articulos",
      name: "Artículos",
      description: "Blogs, posts, trabajos...",
    }
  }

  const renderPage = (
    element: JSX.Element,
    history: MemoryHistory = createMemoryHistory()
  ) => {
    return render(<Router history={history}>{element}</Router>)
  }
})
