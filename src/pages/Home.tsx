import React, { useState, useEffect } from "react"
import { Helmet } from "react-helmet"

import Footer from "../components/Footer"
import CategoryComponent from "../components/Category"
import Header from "../components/Header"

import Api, { Categories, Category } from "../Api"

import "../index.css"

const Home = () => {
  const [categories, updateCategories] = useState<Categories>([])

  const categoriesList = categories.map((category: Category, index: number) => {
    return (
      <div className="column is-narrow" key={index}>
        <CategoryComponent
          id={category.id}
          name={category.name}
          description={category.description}
        />
      </div>
    )
  })

  const fetchCategories = () => {
    const categories = Api.fetchCategories()
    updateCategories(categories)
  }

  useEffect(() => {
    fetchCategories()
  }, [])

  return (
    <>
      <Helmet>
        <title>Bonsai Leaves | Encuentra lo que buscas sobre bonsái</title>
        <meta
          name="description"
          content={`Recopilación de tiendas, escuelas, artículos... sobre Bonsái en español. Encuentra lo que buscas sobre Bonsái.`}
        />
      </Helmet>
      <Header reduced />
      <section className="section">
        <div className="container">
          <div className="column has-text-centered">
            <div className="columns is-multiline is-centered">
              {categoriesList}
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  )
}

export default Home
