import React, { FunctionComponent, useState, useEffect } from "react"
import { Helmet } from "react-helmet"

import Card from "../components/Card"
import Footer from "../components/Footer"
import Header from "../components/Header"
import Hero from "../components/Hero"

import Api, { Resources, Resource, Category } from "../Api"

import "../index.css"

type Props = {
  category: Category
}

const CategoryPage: FunctionComponent<Props> = ({
  category,
}: Props): JSX.Element => {
  const [resources, updateResources] = useState<Resources>([])

  const resourcesList = resources.map((resource: Resource, index) => (
    <Card
      key={index}
      links={resource.links}
      title={resource.name}
      date={resource.date}
      location={resource.location}
    />
  ))

  useEffect(() => {
    if (category === undefined) return

    const resources = Api.fetchResources()
    const filtered = resources.filter((resource) => {
      return resource.categories.indexOf(category.id) >= 0
    })
    updateResources(filtered)
  }, [category])

  return (
    <>
      <Helmet>
        <title>{`${category.name}: ${category.description} | Bonsai Leaves`}</title>
        <meta
          name="description"
          content={`Encuentra en Bonsai Leaves todo lo que busques sobre ${category.name}: ${category.description}`}
        />
      </Helmet>
      <Header reduced />
      <Hero title={category.name} description={category.description} />
      <section className="section">
        <div className="container">
          {category && (
            <div className="columns">
              <div className="column is-three-fifths is-offset-one-fifth has-text-centered">
                <div className="is-medium">{resourcesList}</div>
              </div>
            </div>
          )}
        </div>
      </section>
      <Footer />
    </>
  )
}

export default CategoryPage
