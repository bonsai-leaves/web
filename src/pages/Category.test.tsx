import React from "react"
import { render, screen } from "@testing-library/react"
import CategoryComponent from "./Category"
import { Category, Resource } from "../Api"
import { MemoryRouter } from "react-router-dom"

describe("Category", () => {
  test("renders category title and description", async () => {
    const category = aCategory()

    renderPage(<CategoryComponent category={category} />)

    expect(await screen.queryByText(category.name)).toBeInTheDocument()
    expect(await screen.queryByText(category.description)).toBeInTheDocument()
  })

  test("renders category resources", async () => {
    const category = aCategory()
    const resource = aResourceFrom(category)

    renderPage(<CategoryComponent category={category} />)

    expect(await screen.queryByText(resource.name)).toBeInTheDocument()
    expect(await screen.queryByText(resource.links[0].url)).toBeInTheDocument()
  })

  const aCategory = (): Category => {
    return {
      id: "articulos",
      name: "Artículos",
      description: "Blogs, posts, trabajos...",
    }
  }

  const aResourceFrom = (category: Category): Resource => {
    return {
      name: "El Bonsái y yo - Loli Avilés",
      categories: [category.id],
      links: [
        {
          name: "https://www.lolibonsai.com",
          url: "https://www.lolibonsai.com",
        },
      ],
    }
  }

  const renderPage = (element: JSX.Element) => {
    return render(<MemoryRouter>{element}</MemoryRouter>)
  }
})
