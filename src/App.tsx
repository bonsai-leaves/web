import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Api, { Categories, Category } from "./Api"

import HomePage from "./pages/Home"
import CategoryPage from "./pages/Category"

const App = () => {
  const [categories, updateCategories] = useState<Categories>([])

  const categoryRoutes = categories.map((category: Category) => {
    return (
      <Route key={category.id} exact path={`/${category.id}`}>
        <CategoryPage category={category} />
      </Route>
    )
  })

  useEffect(() => {
    const categories = Api.fetchCategories()
    updateCategories(categories)
  }, [])

  return (
    <Router>
      <Switch>
        {categoryRoutes}
        <Route path="/" component={HomePage} />
      </Switch>
    </Router>
  )
}

export default App
