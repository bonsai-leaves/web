import React, { FunctionComponent } from "react"
import { Link } from "react-router-dom"

type Props = {
  id: string
  name: string
  description: string
}

const Category: FunctionComponent<Props> = (props: Props): JSX.Element => {
  return (
    <Link to={`/${props.id}`} className="box">
      <p className="title is-5">{props.name}</p>
      <p className="subtitle is-6 is-marginless">{props.description}</p>
    </Link>
  )
}

export default Category
