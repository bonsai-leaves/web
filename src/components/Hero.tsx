import React, { FunctionComponent } from "react"

type Props = {
  title: string
  description: string
}

const Hero: FunctionComponent<Props> = (props: Props): JSX.Element => {
  return (
    <section className="hero is-dark is-bold">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">{props.title}</h1>
          <h2 className="subtitle">{props.description}</h2>
        </div>
      </div>
    </section>
  )
}

export default Hero
