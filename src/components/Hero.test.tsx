import React from "react"
import { render, screen } from "@testing-library/react"
import Hero from "./Hero"

describe("Hero", () => {
  test("renders a title and a description", () => {
    const title = "A title"
    const description = "A description"

    render(<Hero title={title} description={description} />)

    expect(screen.queryByText(title)).toBeInTheDocument()
    expect(screen.queryByText(description)).toBeInTheDocument()
  })
})
