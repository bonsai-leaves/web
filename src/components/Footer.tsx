import React, { FunctionComponent } from "react"

const Footer: FunctionComponent = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="columns">
          <div className="column is-8">
            <ul>
              <li>
                <a href="https://gitlab.com/bonsai-leaves/web">
                  Software Libre
                </a>
                .
              </li>
              <li>
                Creado por{" "}
                <a href="https://www.linkedin.com/in/emelgil">Helen</a> y{" "}
                <a href="https://www.linkedin.com/in/vibaiher">Vicente</a>.
              </li>
              <li>
                Con la colaboración de{" "}
                <a href="https://escuelabonsaivalencia.com">
                  Escuela Bonsai Valencia
                </a>
                .
              </li>
            </ul>
          </div>
          <div className="column is-4">
            <ul>
              <li>
                Iconos creados por{" "}
                <a
                  href="https://www.flaticon.com/authors/freepik"
                  title="Freepik"
                >
                  Freepik
                </a>{" "}
                de{" "}
                <a href="https://www.flaticon.com/" title="Flaticon">
                  {" "}
                  www.flaticon.com
                </a>
                .
              </li>
              <li>
                Hecho con <a href="https://bulma.io">Bulma</a>.
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
