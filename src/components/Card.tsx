import React, { FunctionComponent } from "react"

type Props = {
  title: string
  links: { name: string; url: string }[]
  date?: string
  location?: string
}

const Card: FunctionComponent<Props> = (props: Props): JSX.Element => {
  return (
    <div className="box">
      <p className="title is-5">{props.title}</p>
      {props.date && props.location && (
        <p data-testid="date-location" className="subtitle">
          {props.date} - {props.location}
        </p>
      )}
      <div className="subtitle is-6 is-marginless">
        <ul style={{ listStyle: "none" }}>
          {props.links.map((link) => (
            <li key={link.url}>
              <a href={link.url} target="_blank" rel="noopener noreferrer">
                {link.name}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default Card
