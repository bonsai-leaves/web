import React, { FunctionComponent } from "react"
import { Link } from "react-router-dom"

import "../index.css"
import logo from "../images/logo.svg"

type Props = {
  reduced?: boolean
}

const Header: FunctionComponent<Props> = (props: Props): JSX.Element => {
  if (props.reduced === true) {
    return (
      <nav
        className="navbar is-spaced"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              <img src={logo} alt="logo" className="logo" />{" "}
              <h3>BONSAI LEAVES</h3>
            </Link>
          </div>
          <div className="navbar-menu">
            <div className="navbar-end">
              <div className="navbar-item">
                <p>Encuentra lo que buscas sobre bonsai</p>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }

  return (
    <div className="column is-three-fifths is-offset-one-fifth has-text-centered">
      <Link className="content" to="/">
        <img src={logo} alt="logo" className="logo" />
        <h3>BONSAI LEAVES</h3>
      </Link>
      <p>Encuentra lo que buscas sobre bonsai.</p>
    </div>
  )
}

export default Header
