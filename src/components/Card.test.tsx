import React from "react"
import { render, screen } from "@testing-library/react"
import Card from "./Card"

describe("Card", () => {
  test("renders a title and url", () => {
    render(
      <Card
        title="Escuela Bonsai Valencia"
        links={[
          { name: "Web oficial", url: "http://escuelabonsaivalencia.es" },
        ]}
      />
    )

    const title = screen.getByText("Escuela Bonsai Valencia")
    const link = screen.getByText("Web oficial") as HTMLAnchorElement

    expect(title).toBeInTheDocument()
    expect(link).toBeInTheDocument()
    expect(link.href).toBe("http://escuelabonsaivalencia.es/")
  })

  test("shows date and location when present", () => {
    render(
      <Card
        title="II Congreso ABE"
        links={[{ name: "Web oficial", url: "http://www.abebonsai.es" }]}
        date="5 y 6 de Diciembre"
        location="Fuenlabrada, Madrid"
      />
    )

    const dateAndLocation = screen.queryByTestId("date-location")
    expect(dateAndLocation).toBeInTheDocument()
  })

  test("accepts multiple links", () => {
    const links = [
      { name: "a link title", url: "http://example.org/" },
      { name: "other link", url: "http://example.org/two/" },
    ]

    render(
      <Card
        title="Kokufu Ten"
        links={links}
        date="febrero"
        location="Tokyo, Japón"
      />
    )

    links.forEach((link) => {
      const anchor = screen.queryByText(link.name) as HTMLElement
      expect(anchor).toBeInTheDocument()
      expect(anchor.getAttribute("href")).toBe(link.url)
    })
  })
})
