const categories = [
  {
    id: "articulos",
    name: "Artículos",
    description: "Blogs, posts, trabajos...",
  },
  {
    id: "asociaciones",
    name: "Asociaciones",
    description: "Grupos de profesionales, aficionados...",
  },
  {
    id: "ceramistas",
    name: "Ceramistas",
    description: "Macetas, kuramas...",
  },
  {
    id: "escuelas",
    name: "Escuelas",
    description: "Encuentra sitios donde aprender",
  },
  {
    id: "exposiciones",
    name: "Exposiciones",
    description: "Congresos y exposiciones",
  },
  {
    id: "tiendas",
    name: "Tiendas",
    description: "Bonsai, prebonsai, macetas, herramientas...",
  },
  {
    id: "videos",
    name: "Vídeos",
    description: "Canales de youtube, tutoriales...",
  },
];

export default categories;
