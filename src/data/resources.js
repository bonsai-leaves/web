const resources = [
  {
    name: "Escuela Bonsai Valencia",
    categories: ["escuelas"],
    links: [
      {
        name: "http://escuelabonsaivalencia.es",
        url: "http://escuelabonsaivalencia.es",
      },
    ],
  },
  {
    name: "Centro Bonsai, Alboraya",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.centrobonsai.com",
        url: "https://www.centrobonsai.com",
      },
    ],
  },
  {
    name: "El TIM Bonsai",
    categories: ["escuelas"],
    links: [
      {
        name: "https://www.facebook.com/eltim.bonsai",
        url: "https://www.facebook.com/eltim.bonsai",
      },
    ],
  },
  {
    name: "Asociación del Bonsai Español",
    categories: ["asociaciones"],
    links: [
      {
        name: "https://www.facebook.com/ABEBonsai",
        url: "https://www.facebook.com/ABEBonsai",
      },
    ],
  },
  {
    name: "Asociación Española de Bonsái",
    categories: ["asociaciones"],
    links: [
      {
        name: "https://www.facebook.com/aebonsai",
        url: "https://www.facebook.com/aebonsai",
      },
    ],
  },
  {
    name: "Unión del Bonsai Español",
    categories: ["asociaciones"],
    links: [
      {
        name: "https://ubebonsai.es",
        url: "https://ubebonsai.es",
      },
    ],
  },
  {
    name: "El Bonsái y yo - Loli Avilés",
    categories: ["articulos"],
    links: [
      {
        name: "https://www.lolibonsai.com",
        url: "https://www.lolibonsai.com",
      },
    ],
  },
  {
    name: "Trabajos de la Escuela Bonsai Valencia",
    categories: ["articulos"],
    links: [
      {
        name: "http://escuelabonsaivalencia.es/categorias/",
        url: "http://escuelabonsaivalencia.es/categorias/",
      },
    ],
  },
  {
    name: "Bonsai Nostrum",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.bonsainostrum.com",
        url: "https://www.bonsainostrum.com",
      },
    ],
  },
  {
    name: "Bonsai Granada",
    categories: ["escuelas", "tiendas"],
    links: [
      {
        name: "https://www.bonsaigranada.com",
        url: "https://www.bonsaigranada.com",
      },
    ],
  },
  {
    name: "Bonsái Pavía",
    categories: ["escuelas"],
    links: [
      {
        name: "https://www.bonsaipavia.es",
        url: "https://www.bonsaipavia.es",
      },
    ],
  },
  {
    name: "Medibonsai - Bonsai del Mediterraneo",
    categories: ["tiendas"],
    links: [
      {
        name: "http://medibonsai.com",
        url: "http://medibonsai.com",
      },
    ],
  },
  {
    name: "Gabriel Romero - Bonsaisantboi",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.facebook.com/gabrielromerobonsaisantboi",
        url: "https://www.facebook.com/gabrielromerobonsaisantboi",
      },
    ],
  },
  {
    name: "Studio Bonsai David Quintana",
    categories: ["escuelas", "tiendas"],
    links: [
      {
        name: "https://dqbonsai.com",
        url: "https://dqbonsai.com",
      },
    ],
  },
  {
    name: "Estudio Bonsai David Benavente",
    categories: ["escuelas", "tiendas"],
    links: [
      {
        name: "https://davidbenavente.com",
        url: "https://davidbenavente.com",
      },
    ],
  },
  {
    name: "Bonsai Sense",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.bonsaisense.com",
        url: "https://www.bonsaisense.com",
      },
    ],
  },
  {
    name: "Bonsai Empire",
    categories: ["escuelas"],
    links: [
      {
        name: "https://www.bonsaiempire.es",
        url: "https://www.bonsaiempire.es",
      },
    ],
  },
  {
    name: "Kingii | Bonsáis",
    categories: ["tiendas", "escuelas"],
    links: [
      {
        name: "https://www.kingii.es",
        url: "https://www.kingii.es",
      },
    ],
  },
  {
    name: "Bonsais La Perla",
    categories: ["tiendas"],
    links: [
      {
        name: "https://bonsaislaperla.net",
        url: "https://bonsaislaperla.net",
      },
    ],
  },
  {
    name: "Bonsai Colmenar",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.bonsaicolmenar.com",
        url: "https://www.bonsaicolmenar.com",
      },
    ],
  },
  {
    name: "Mistral Bonsai",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.mistralbonsai.com",
        url: "https://www.mistralbonsai.com",
      },
    ],
  },
  {
    name: "Lombrico Bonsai",
    categories: ["tiendas"],
    links: [
      {
        name: "http://lombricobonsai.com",
        url: "http://lombricobonsai.com",
      },
    ],
  },
  {
    name: "Bonsaikido",
    categories: ["tiendas", "escuelas"],
    links: [
      {
        name: "https://bonsaikido.com",
        url: "https://bonsaikido.com",
      },
    ],
  },
  {
    name: "Bonsai Levante",
    categories: ["escuelas"],
    links: [
      {
        name: "https://www.bonsailevante.com",
        url: "https://www.bonsailevante.com",
      },
    ],
  },
  {
    name: "Naturdecora",
    categories: ["tiendas"],
    links: [
      {
        name: "https://naturdecora.com",
        url: "https://naturdecora.com",
      },
    ],
  },
  {
    name: "Mercadillo Mercabonsai",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.facebook.com/groups/1417529361826180",
        url: "https://www.facebook.com/groups/1417529361826180",
      },
    ],
  },
  {
    name: "El rastrillo del bonsai",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.facebook.com/groups/1738600986238832",
        url: "https://www.facebook.com/groups/1738600986238832",
      },
    ],
  },
  {
    name: "El Mercadillo del Bonsai",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.facebook.com/groups/1738600986238832",
        url: "https://www.facebook.com/groups/1738600986238832",
      },
    ],
  },
  {
    name: "El Kumi Bonsai",
    categories: ["escuelas"],
    links: [
      {
        name: "https://www.facebook.com/el.kumi.7",
        url: "https://www.facebook.com/el.kumi.7",
      },
    ],
  },
  {
    name: "Bonsái Jaume Canals",
    categories: ["tiendas", "escuelas"],
    links: [
      {
        name: "https://www.jaumecanals.com",
        url: "https://www.jaumecanals.com",
      },
    ],
  },
  {
    name: "Jose Antonio Guerao",
    categories: ["ceramistas"],
    links: [
      {
        name: "https://www.facebook.com/joseantonio.gueraonavarro",
        url: "https://www.facebook.com/joseantonio.gueraonavarro",
      },
    ],
  },
  {
    name: "Maria José González Rodríguez",
    categories: ["ceramistas"],
    links: [
      {
        name: "https://www.facebook.com/profile.php?id=100009507237081",
        url: "https://www.facebook.com/profile.php?id=100009507237081",
      },
    ],
  },
  {
    name: "KIRO Ceramics",
    categories: ["ceramistas"],
    links: [
      {
        name: "https://www.facebook.com/KIROCeramics/",
        url: "https://www.facebook.com/KIROCeramics/",
      },
    ],
  },
  {
    name: "Laos Garden",
    categories: ["tiendas"],
    links: [
      {
        name: "https://www.laosgarden.com/es",
        url: "https://www.laosgarden.com/es",
      },
    ],
  },
  {
    name: "Crataegus Bonsai Cofrentes",
    categories: ["asociaciones"],
    links: [
      {
        name: "https://www.facebook.com/groups/bonsaicofrentes2",
        url: "https://www.facebook.com/groups/bonsaicofrentes2",
      },
    ],
  },
  {
    name: "II Congreso ABE",
    categories: ["exposiciones"],
    date: "5 y 6 de diciembre 2020",
    location: "Fuenlabrada, Madrid",
    links: [
      {
        name: "http://www.abebonsai.es",
        url: "http://www.abebonsai.es",
      },
    ],
  },
  {
    name: "III Convención UBEbonsai",
    categories: ["exposiciones"],
    date: "29, 30 y 31 de enero 2021",
    location: "Aranjuez, Madrid",
    links: [
      {
        name: "https://ubebonsai.es/index.php/iii-ubebonsai",
        url: "https://ubebonsai.es/index.php/iii-ubebonsai",
      },
    ],
  },
  {
    name: "Alberto Gimeno, pinos thumbergii desde semilla",
    categories: ["videos"],
    links: [
      {
        name: "https://youtu.be/FCRshqKQCNE",
        url: "https://youtu.be/FCRshqKQCNE",
      },
    ],
  },
  {
    name: "Luis Vila nos enseña como hace los pinos silvestris desde plantón",
    categories: ["videos"],
    links: [
      {
        name: "https://youtu.be/zkDGo77JrYo",
        url: "https://youtu.be/zkDGo77JrYo",
      },
    ],
  },
  {
    name: "Aleixo Seixo",
    categories: ["ceramistas"],
    links: [
      {
        name: "https://www.facebook.com/aleixo.seixo",
        url: "https://www.facebook.com/aleixo.seixo",
      },
    ],
  },
  {
    name: "Iñaki Tellería",
    categories: ["ceramistas"],
    links: [
      {
        name: "https://www.facebook.com/inaki.telleria.96",
        url: "https://www.facebook.com/inaki.telleria.96",
      },
    ],
  },
  {
    name: "Gardencenter La Pascualeta, Valencia",
    categories: ["tiendas"],
    links: [
      {
        name: "https://gclapascualeta.com",
        url: "https://gclapascualeta.com",
      },
    ],
  },
  {
    name: "94th Kokufu Bonsai Exhibition 2020",
    categories: ["exposiciones"],
    date: "8-11 y 13-16 de febrero 2020",
    location: "Tokyo, Japón",
    links: [
      {
        name: "Página oficial",
        url: "http://www.bonsai-kyokai.or.jp/kokuhuten.htm",
      },
      {
        name: "Fotos de los premiados",
        url:
          "https://bonsaitonight.com/2020/02/21/prize-winning-trees-from-the-94th-kokufu-bonsai-exhibition/",
      },
      {
        name: "Fotos de los acentos",
        url:
          "https://bonsaitonight.com/2020/02/25/accents-displayed-at-the-94th-kokufu-ten/",
      },
      {
        name: "Video resumen",
        url: "https://www.youtube.com/watch?v=wGrM6HMkiPg",
      },
    ],
  },
];

export default resources;
