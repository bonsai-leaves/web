# Bonsai Web

Web to search bonsai resources

## Requisites

- Docker Engine - Community (>= 19.03.8)
- docker-compose (version 1.25.3)
- Compatible linux terminal

## Available Scripts

In the project directory, you can run:

### `scripts/start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `scripts/test`

Runs all tests.

## Colors

Logo/icon contains these color palette:

```
black: #000000
purple-light: #E0C9ED
purple: #C193DC
brown: #DDB9A3
green: #B8ECBC
green-light: #D0F2D2
```
