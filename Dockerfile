FROM node:14.16.0-alpine3.12

ENV APP /app
WORKDIR $APP

COPY package.json package-lock.json $APP/
RUN npm install

COPY . .

ENV PORT 3000
EXPOSE $PORT

CMD ["npm", "start"]
